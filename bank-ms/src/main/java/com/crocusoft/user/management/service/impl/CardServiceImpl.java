package com.crocusoft.user.management.service.impl;

import com.crocusoft.user.management.domain.Card;
import com.crocusoft.user.management.exception.CardBalanceNotEnoughException;
import com.crocusoft.user.management.exception.CardExpiredExceptions;
import com.crocusoft.user.management.exception.CardNotFoundException;
import com.crocusoft.user.management.exception.InvalidCardDetailsExceptions;
import com.crocusoft.user.management.repository.CardRepository;
import com.crocusoft.user.management.service.CardService;
import com.crocusoft.user.management.service.dto.CardRequestDto;
import com.crocusoft.user.management.service.dto.CardResponseDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;
    private final ModelMapper modelMapper;

    @Override
    public CardResponseDto deposit(CardRequestDto cardRequestDto) {
        Card card = checkCardIsPresent(cardRequestDto);
        checkCardDetails(card, cardRequestDto);
        card.setBalance(card.getBalance() + cardRequestDto.getAmount());
        return modelMapper.map(cardRepository.save(card), CardResponseDto.class);
    }

    @Override
    public CardResponseDto withdraw(CardRequestDto cardRequestDto) {
        Card card = checkCardIsPresent(cardRequestDto);
        checkCardDetails(card, cardRequestDto);
        if (card.getBalance() >= cardRequestDto.getAmount()) {
            card.setBalance(card.getBalance() - cardRequestDto.getAmount());
            return modelMapper.map(cardRepository.save(card), CardResponseDto.class);
        } else {
            throw new CardBalanceNotEnoughException(cardRequestDto.getCardNumber());
        }
    }

    private Card checkCardIsPresent(CardRequestDto cardRequestDto) {
        return cardRepository.findByCardNumber(cardRequestDto.getCardNumber())
                .orElseThrow(() -> new CardNotFoundException(cardRequestDto.getCardNumber().toString()));
    }

    private void checkCardDetails(Card card, CardRequestDto cardRequestDto) {

        if (card.getExpiryDate().isBefore(LocalDateTime.now())) {
            throw new CardExpiredExceptions(card.getCardNumber());
        } else if (!card.getCvv().equals(cardRequestDto.getCvv())) {
            throw new InvalidCardDetailsExceptions();
        }
    }

}
