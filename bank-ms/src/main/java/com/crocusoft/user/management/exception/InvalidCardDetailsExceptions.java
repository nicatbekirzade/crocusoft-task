package com.crocusoft.user.management.exception;


import com.crocusoft.common.exception.InvalidStateException;

public class InvalidCardDetailsExceptions extends InvalidStateException {

    private static final long serialVersionUID = 58432132465811L;

    public InvalidCardDetailsExceptions() {
        super("Card details are not valid, please try again");
    }
}
