package com.crocusoft.user.management.domain;

import com.crocusoft.user.management.domain.enumeration.CardStatus;
import com.crocusoft.user.management.domain.enumeration.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Card.TABLE_NAME)
public class Card {

    public static final String TABLE_NAME = "cards";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Length(min = 16, max = 16)
    private Long cardNumber;

    @DateTimeFormat(pattern = "YY/MM")
    private LocalDateTime expiryDate;

    @Length(min = 3, max = 3, message = "Invalid cvv format")
    private String cvv;

    private Currency currency;

    private Double balance;

    private CardStatus status;

    private String password;
}
