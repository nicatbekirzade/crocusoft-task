package com.crocusoft.user.management.service.impl;

import com.crocusoft.user.management.domain.Card;
import com.crocusoft.user.management.domain.Customer;
import com.crocusoft.user.management.domain.enumeration.CardStatus;
import com.crocusoft.user.management.domain.enumeration.Currency;
import com.crocusoft.user.management.repository.CustomerRepository;
import com.crocusoft.user.management.service.CustomerService;
import com.crocusoft.user.management.service.dto.CustomerRequestDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;

import static net.andreinc.mockneat.unit.financial.CVVS.cvvs;
import static net.andreinc.mockneat.unit.financial.CreditCards.creditCards;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final ModelMapper modelMapper;

    @Override
    public void create(CustomerRequestDto customerRequestDto) {
        Customer customer = modelMapper.map(customerRequestDto, Customer.class);
        Card card = createCardDetails();
        card.setPassword(customerRequestDto.getCardDto().getPassword());
        customer.setCards(Set.of(card));
        customerRepository.save(customer);
    }

    private Card createCardDetails() {
        return Card.builder()
                .cardNumber(Long.getLong(creditCards().visa().get()))
                .cvv(cvvs().get())
                .status(CardStatus.ACTIVE)
                .currency(Currency.AZN)
                .expiryDate(LocalDateTime.now().plusYears(1L))
                .build();
    }

}
