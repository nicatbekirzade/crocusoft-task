package com.crocusoft.user.management.domain.enumeration;

public enum CardStatus {
    ACTIVE, BLOCKED
}
