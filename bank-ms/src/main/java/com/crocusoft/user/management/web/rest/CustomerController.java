package com.crocusoft.user.management.web.rest;

import com.crocusoft.user.management.service.CustomerService;
import com.crocusoft.user.management.service.dto.CustomerRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customers")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Validated CustomerRequestDto customerRequestDto) {
        log.trace("Create customers: {}", customerRequestDto);

        customerService.create(customerRequestDto);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
