package com.crocusoft.user.management.config;

import com.corcusoft.common.security.auth.service.SecurityService;
import com.crocusoft.common.config.ModelMapperConfig;
import com.crocusoft.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ModelMapperConfig.class, GlobalExceptionHandler.class})
public class CommonConfig {

    @Bean
    public SecurityService getSecurityService() {
        return new SecurityService();
    }

}
