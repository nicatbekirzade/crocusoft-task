package com.crocusoft.user.management.service;

import com.crocusoft.user.management.service.dto.CardRequestDto;
import com.crocusoft.user.management.service.dto.CardResponseDto;

public interface CardService {
    CardResponseDto deposit(CardRequestDto cardRequestDto);

    CardResponseDto withdraw(CardRequestDto cardRequestDto);
}

