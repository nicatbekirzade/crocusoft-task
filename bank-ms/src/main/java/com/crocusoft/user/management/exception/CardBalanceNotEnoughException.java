package com.crocusoft.user.management.exception;


import com.crocusoft.common.exception.InvalidStateException;

public class CardBalanceNotEnoughException extends InvalidStateException {

    private static final long serialVersionUID = 58432632465811L;

    public CardBalanceNotEnoughException() {
        super("Not enough balance");
    }

    public CardBalanceNotEnoughException(Long cardNumber) {
        super(String.format("Card %s has not enough balance", cardNumber));
    }
}
