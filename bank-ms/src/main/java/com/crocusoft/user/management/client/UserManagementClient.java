package com.crocusoft.user.management.client;

import com.crocusoft.common.dto.UserCommonResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${client.users.name}", url = "${client.users.url}")
public interface UserManagementClient {

    @GetMapping("/v1/users/check")
    ResponseEntity<Boolean> checkUsersExist(@RequestHeader("Authorization") String authorization,
                                            @RequestParam("id") List<Long> ids);

    @GetMapping("/v1/users/bulk")
    ResponseEntity<List<UserCommonResponseDto>> getUsersById(@RequestHeader("Authorization") String authorization,
                                                             @RequestParam("id") List<Long> ids);

}
