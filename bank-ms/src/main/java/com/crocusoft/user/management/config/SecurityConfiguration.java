package com.crocusoft.user.management.config;

import com.corcusoft.common.security.UserAuthority;
import com.corcusoft.common.security.auth.AuthenticationEntryPointConfigurer;
import com.corcusoft.common.security.auth.service.AuthService;
import com.corcusoft.common.security.auth.service.JwtService;
import com.corcusoft.common.security.config.BaseSecurityConfig;
import com.corcusoft.common.security.config.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.List;

@Slf4j
@Import({SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class})
@EnableWebSecurity
public class SecurityConfiguration extends BaseSecurityConfig {

    private static final String CUSTOMERS = "/v1/customers/**"; //TODO access authorities

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, CUSTOMERS).access(authorities(UserAuthority.ADMIN, UserAuthority.USER))
                .antMatchers(HttpMethod.PUT, CUSTOMERS).access(authorities(UserAuthority.ADMIN, UserAuthority.USER))
                .antMatchers(HttpMethod.DELETE, CUSTOMERS).access(authorities(UserAuthority.ADMIN, UserAuthority.USER))
                .antMatchers(HttpMethod.GET).access(authorities(UserAuthority.ADMIN, UserAuthority.USER));

        super.configure(http);
    }

}
