package com.crocusoft.user.management.exception;


import com.crocusoft.common.exception.InvalidStateException;

public class CardExpiredExceptions extends InvalidStateException {

    private static final long serialVersionUID = 58432532465811L;

    public CardExpiredExceptions() {
        super("Card activity expired");
    }

    public CardExpiredExceptions(Long cardNumber) {
        super(String.format("Card %s activity expired", cardNumber));
    }
}
