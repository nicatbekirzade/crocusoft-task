package com.crocusoft.user.management.web.rest;

import com.crocusoft.user.management.service.CardService;
import com.crocusoft.user.management.service.dto.CardRequestDto;
import com.crocusoft.user.management.service.dto.CardResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/cards")
public class CardController {

    private final CardService cardService;

    @PutMapping("/deposit")
    public ResponseEntity<CardResponseDto> deposit(@RequestBody @Validated CardRequestDto cardRequestDto) {
        log.trace("Deposit to card: {}", cardRequestDto.getCardNumber());
        return ResponseEntity.ok(cardService.deposit(cardRequestDto));
    }

    @PutMapping("/withdraw")
    public ResponseEntity<CardResponseDto> withdraw(@RequestBody @Validated CardRequestDto cardRequestDto) {
        log.trace("Withdraw from card: {}", cardRequestDto.getCardNumber());
        return ResponseEntity.ok(cardService.withdraw(cardRequestDto));
    }

}
