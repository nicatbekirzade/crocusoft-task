package com.crocusoft.user.management.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardRequestDto {

    @NotBlank
    private Long cardNumber;

    @Length(min = 3, max = 3, message = "Invalid cvv format")
    @NotBlank
    private String cvv;

    @NotBlank
    private String password;

    @Positive
    @NotBlank
    private Double amount;

}
