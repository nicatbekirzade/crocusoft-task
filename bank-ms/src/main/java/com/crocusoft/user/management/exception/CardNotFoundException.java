package com.crocusoft.user.management.exception;

import com.crocusoft.common.exception.NotFoundException;

public class CardNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 8081L;

    public CardNotFoundException(String message) {
        super(message);
    }
}
