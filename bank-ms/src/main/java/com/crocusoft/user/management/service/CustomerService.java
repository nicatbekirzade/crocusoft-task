package com.crocusoft.user.management.service;

import com.crocusoft.user.management.service.dto.CustomerRequestDto;

public interface CustomerService {
    void create(CustomerRequestDto customerRequestDto);
}

