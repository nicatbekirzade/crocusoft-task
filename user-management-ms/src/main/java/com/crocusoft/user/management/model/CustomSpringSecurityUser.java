package com.crocusoft.user.management.model;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomSpringSecurityUser extends User {

    private static final long serialVersionUID = 3522416053866116034L;

    public CustomSpringSecurityUser(String username, String password,
                                    Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}
