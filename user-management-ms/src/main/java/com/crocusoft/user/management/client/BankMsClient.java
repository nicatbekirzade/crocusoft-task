package com.crocusoft.user.management.client;

import com.crocusoft.user.management.service.dto.card.dtos.CardRequestDto;
import com.crocusoft.user.management.service.dto.card.dtos.CardResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "${client.cards.name}", url = "${client.cards.url}")
public interface BankMsClient {

    @PutMapping("/v1/cards/deposit")
    ResponseEntity<CardResponseDto> deposit(@RequestHeader("Authorization") String authorization,
                                            @RequestBody @Validated CardRequestDto cardRequestDto);

    @PutMapping("/v1/cards/withdraw")
    ResponseEntity<CardResponseDto> withdraw(@RequestHeader("Authorization") String authorization,
                                             @RequestBody @Validated CardRequestDto cardRequestDto);

}
