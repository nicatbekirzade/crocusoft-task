package com.crocusoft.user.management.exception;

import com.crocusoft.common.exception.NotFoundException;

public class ShoppingNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 8081L;

    public ShoppingNotFoundException(String message) {
        super(message);
    }
}
