package com.crocusoft.user.management.service;

import com.crocusoft.user.management.service.dto.card.dtos.CardRequestDto;

public interface ShoppingService {


    void shop(Long id, int count, Double amount, CardRequestDto cardRequestDto);
}
