package com.crocusoft.user.management.service.dto;

import com.crocusoft.user.management.config.AuthConstants;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ManagedUserDto {

    @NotBlank
    private String fullName;

    @NotBlank
    @Size(min = AuthConstants.USERNAME_MIN_LENGTH, max = AuthConstants.USERNAME_MAX_LENGTH)
    private String username;

    @Email
    private String email;

    @Size(max = AuthConstants.PASSWORD_MAX_LENGTH, min = AuthConstants.PASSWORD_MIN_LENGTH)
    private String password;

    @NotBlank
    private String phone;

}
