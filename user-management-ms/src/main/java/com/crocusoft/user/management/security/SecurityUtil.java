package com.crocusoft.user.management.security;

import com.corcusoft.common.security.auth.service.JwtService;
import com.corcusoft.common.security.auth.service.SecurityService;
import com.corcusoft.common.security.exception.UserNotFoundException;
import com.crocusoft.user.management.domain.Authority;
import com.crocusoft.user.management.domain.User;
import com.crocusoft.user.management.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Import(SecurityService.class)
public class SecurityUtil {

    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_WEEK = Duration.ofDays(7);

    private final UserRepository userRepository;
    private final SecurityService securityService;
    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public User getCurrentUser() {
        return securityService.getCurrentUserLogin()
                .map(userRepository::findUserByUsernameIgnoreCase)
                .map(Optional::get)
                .orElseThrow(UserNotFoundException::new);
    }

    public String createAuthentication(String username, String password, Boolean rememberMe) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return jwtService.issueToken(authentication, getDuration(rememberMe));
    }

    public Set<Authority> createAuthorities(String... authoritiesString) {
        Set<Authority> authorities = new HashSet<>();
        for (String authorityString : authoritiesString) {
            authorities.add(new Authority(authorityString));
        }
        return authorities;
    }

    private Duration getDuration(Boolean rememberMe) {
        return ((rememberMe != null) && rememberMe) ? ONE_WEEK : ONE_DAY;
    }

}
