package com.crocusoft.user.management.service.impl;

import com.corcusoft.common.security.UserAuthority;
import com.corcusoft.common.security.exception.UserNotFoundException;
import com.crocusoft.user.management.domain.User;
import com.crocusoft.user.management.domain.enumeration.UserStatus;
import com.crocusoft.user.management.repository.UserRepository;
import com.crocusoft.user.management.security.SecurityUtil;
import com.crocusoft.user.management.service.UserService;
import com.crocusoft.user.management.service.dto.ManagedUserDto;
import com.crocusoft.user.management.service.dto.UserResponseDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;
    private final SecurityUtil securityUtil;

    @Override
    @Transactional
    public void create(ManagedUserDto managedUserDto) {
        User user = modelMapper.map(managedUserDto, User.class);
        user.setAuthorities(securityUtil.createAuthorities(UserAuthority.USER.name()));
        user.setStatus(UserStatus.ACTIVE);
        user.setPassword(passwordEncoder.encode(managedUserDto.getPassword()));
        userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponseDto> getAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(user -> modelMapper.map(user, UserResponseDto.class));
    }

    @Override
    @Transactional(readOnly = true)
    public UserResponseDto getById(Long id) {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return modelMapper.map(user, UserResponseDto.class);
    }

}
