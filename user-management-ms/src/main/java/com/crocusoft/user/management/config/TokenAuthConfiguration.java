package com.crocusoft.user.management.config;


import com.corcusoft.common.security.auth.service.JwtService;
import com.corcusoft.common.security.auth.service.TokenAuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenAuthConfiguration {

    @Bean
    public TokenAuthService tokenAuthService(JwtService jwtService) {
        return new TokenAuthService(jwtService);
    }
}
