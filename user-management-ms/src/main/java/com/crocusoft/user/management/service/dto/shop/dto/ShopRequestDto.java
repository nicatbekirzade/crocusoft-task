package com.crocusoft.user.management.service.dto.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopRequestDto {


    @Positive
    @NotBlank
    private int count;

    @Positive
    @NotBlank
    private Double amount;

}
