package com.crocusoft.user.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserManagementMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

}
