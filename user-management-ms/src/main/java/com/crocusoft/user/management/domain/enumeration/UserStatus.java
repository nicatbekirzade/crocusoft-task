package com.crocusoft.user.management.domain.enumeration;

public enum UserStatus {
    ACTIVE, DELETED, BLOCKED
}
