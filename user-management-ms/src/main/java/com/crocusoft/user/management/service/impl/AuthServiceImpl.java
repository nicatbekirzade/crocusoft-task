package com.crocusoft.user.management.service.impl;

import com.corcusoft.common.security.UserAuthority;
import com.crocusoft.user.management.domain.User;
import com.crocusoft.user.management.domain.enumeration.UserStatus;
import com.crocusoft.user.management.repository.UserRepository;
import com.crocusoft.user.management.security.SecurityUtil;
import com.crocusoft.user.management.service.AuthService;
import com.crocusoft.user.management.service.dto.AdminRegisterDto;
import com.crocusoft.user.management.service.dto.JwtTokenDto;
import com.crocusoft.user.management.service.dto.LoginDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final SecurityUtil securityUtil;

    @Override
    @Transactional
    public void register(AdminRegisterDto adminRegisterDto) {
        User user = modelMapper.map(adminRegisterDto, User.class);

        user.setAuthorities(securityUtil.createAuthorities(UserAuthority.ADMIN.name()));
        user.setStatus(UserStatus.ACTIVE);
        user.setPassword(passwordEncoder.encode(adminRegisterDto.getPassword()));

        userRepository.save(user);
    }

    @Override
    public JwtTokenDto login(LoginDto loginDto) {
        String token = securityUtil
                .createAuthentication(loginDto.getUsername(), loginDto.getPassword(), loginDto.getRememberMe());

        return JwtTokenDto.builder().idToken(token).build();
    }

}
