package com.crocusoft.user.management.service.impl;

import com.crocusoft.common.exception.NotFoundException;
import com.crocusoft.user.management.client.BankMsClient;
import com.crocusoft.user.management.exception.ShoppingCountHasNotEnoughException;
import com.crocusoft.user.management.exception.ShoppingNotFoundException;
import com.crocusoft.user.management.repository.ShoppingRepository;
import com.crocusoft.user.management.service.ShoppingService;
import com.crocusoft.user.management.service.dto.card.dtos.CardRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ShoppingServiceImpl implements ShoppingService {

    private final HttpServletRequest servletRequest;
    private final ShoppingRepository shoppingRepository;
    private final BankMsClient bankClient;

    @Override
    @Transactional
    public void shop(Long id, int count, Double amount, CardRequestDto cardRequestDto) {
        shoppingRepository.findById(id)
                .map(shopping1 -> {
                    if (shopping1.getCount() <= count) {
                        throw new ShoppingCountHasNotEnoughException(
                                String.format("There are not left %s from the thing with id: %s", count, id));
                    } else {
                        return shopping1;
                    }
                })
                .orElseThrow(() -> {
                    throw new ShoppingNotFoundException(String.format("Things with this id: %s doesn't exist. ", id));
                });
        cardRequestDto.setAmount(amount);
        try {
            bankClient.withdraw(getAuthorizationHeader(), cardRequestDto);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(String.format("Entry with id %s not found", id), ex);
        }
    }

    private String getAuthorizationHeader() {
        return servletRequest.getHeader("Authorization");
    }

}
