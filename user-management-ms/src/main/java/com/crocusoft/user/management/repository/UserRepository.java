package com.crocusoft.user.management.repository;

import com.crocusoft.user.management.domain.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(attributePaths = "authorities")
    @Query("select u from User u where lower(u.username)=lower(:username)")
    Optional<User> findOneWithAuthoritiesByUsername(@Param("username") String login);

    Optional<User> findUserByUsernameIgnoreCase(String username);

}
