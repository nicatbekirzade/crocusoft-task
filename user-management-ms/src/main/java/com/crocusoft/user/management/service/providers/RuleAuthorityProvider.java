package com.crocusoft.user.management.service.providers;

import com.corcusoft.common.security.auth.service.Claim;
import com.corcusoft.common.security.auth.service.ClaimProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RuleAuthorityProvider implements ClaimProvider {

    private static final String RULE = "rule";

    @Override
    public Claim provide(Authentication authentication) {
        log.trace("Providing claims");
        Set<String> authorities = authentication.getAuthorities().stream()
                .map(Object::toString)
                .collect(Collectors.toSet());
        return new Claim(RULE, authorities);
    }
}
