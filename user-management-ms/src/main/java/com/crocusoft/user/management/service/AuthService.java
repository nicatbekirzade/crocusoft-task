package com.crocusoft.user.management.service;

import com.crocusoft.user.management.service.dto.AdminRegisterDto;
import com.crocusoft.user.management.service.dto.JwtTokenDto;
import com.crocusoft.user.management.service.dto.LoginDto;

public interface AuthService {

    void register(AdminRegisterDto adminRegisterDto);

    JwtTokenDto login(LoginDto loginDto);

}
