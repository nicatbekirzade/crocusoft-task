package com.crocusoft.user.management.service;

import com.crocusoft.user.management.service.dto.ManagedUserDto;
import com.crocusoft.user.management.service.dto.UserResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    void create(ManagedUserDto userVm);

    Page<UserResponseDto> getAll(Pageable pageable);

    UserResponseDto getById(Long id);

}
