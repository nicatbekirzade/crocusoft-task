package com.crocusoft.user.management.config;

import com.crocusoft.common.config.ModelMapperConfig;
import com.crocusoft.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({GlobalExceptionHandler.class, ModelMapperConfig.class})
@Configuration
public class CommonConfig {
}
