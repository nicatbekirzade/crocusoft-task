package com.crocusoft.user.management.web.rest;

import com.crocusoft.user.management.service.AuthService;
import com.crocusoft.user.management.service.dto.AdminRegisterDto;
import com.crocusoft.user.management.service.dto.JwtTokenDto;
import com.crocusoft.user.management.service.dto.LoginDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/auth")
public class AuthController {

    private final AuthService userService;

    @PostMapping("/login")
    public ResponseEntity<JwtTokenDto> login(@RequestBody @Validated LoginDto loginDto) {
        log.trace("Authenticating user {}", loginDto.getUsername());

        return ResponseEntity.ok(userService.login(loginDto));
    }

    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody @Validated AdminRegisterDto adminRegisterDto) {
        log.trace("Authenticating user {}", adminRegisterDto.getUsername());

        userService.register(adminRegisterDto);
        return ResponseEntity.ok().build();
    }

}
