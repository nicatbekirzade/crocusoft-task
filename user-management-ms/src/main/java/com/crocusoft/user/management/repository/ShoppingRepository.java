package com.crocusoft.user.management.repository;

import com.crocusoft.user.management.domain.Shopping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingRepository extends JpaRepository<Shopping, Long> {


}
