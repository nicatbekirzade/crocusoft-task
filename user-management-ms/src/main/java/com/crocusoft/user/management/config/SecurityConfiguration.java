package com.crocusoft.user.management.config;

import com.corcusoft.common.security.UserAuthority;
import com.corcusoft.common.security.auth.AuthenticationEntryPointConfigurer;
import com.corcusoft.common.security.auth.service.JwtService;
import com.corcusoft.common.security.auth.service.TokenAuthService;
import com.corcusoft.common.security.config.BaseSecurityConfig;
import com.corcusoft.common.security.config.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends BaseSecurityConfig {

    public SecurityConfiguration(SecurityProperties securityProperties, JwtService jwtService) {
        super(securityProperties, List.of(new TokenAuthService(jwtService)));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/v1/auth/**").permitAll()
                .antMatchers("/v1/users/**").access(authorities(UserAuthority.ADMIN));
        super.configure(http);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
