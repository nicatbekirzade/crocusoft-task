package com.crocusoft.user.management.service.dto;

import com.crocusoft.user.management.config.AuthConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "password")
public class AdminRegisterDto {

    @NotBlank
    private String fullName;

    @NotBlank
    @Size(min = AuthConstants.USERNAME_MIN_LENGTH, max = AuthConstants.USERNAME_MAX_LENGTH)
    private String username;

    @Email
    private String email;

    @Size(max = AuthConstants.PASSWORD_MAX_LENGTH, min = AuthConstants.PASSWORD_MIN_LENGTH)
    private String password;

    @NotBlank
    private String phone;


}
