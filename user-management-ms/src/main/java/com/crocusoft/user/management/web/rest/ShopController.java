package com.crocusoft.user.management.web.rest;

import com.crocusoft.user.management.service.ShoppingService;
import com.crocusoft.user.management.service.dto.card.dtos.CardRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/shop")
public class ShopController {

    private ShoppingService shoppingService;

    @PutMapping("/{id}")
    public ResponseEntity<Void> shop(@PathVariable Long id,
                                     @RequestParam("amount") Double amount,
                                     @RequestParam(name = "count", defaultValue = "1") int count,
                                     @RequestBody CardRequestDto cardRequestDto) {
        log.trace("Shops : {} ,count : {} ,amount: {}", id, count, amount);

        shoppingService.shop(id, count, amount, cardRequestDto);

        return ResponseEntity.ok().build();
    }

}
