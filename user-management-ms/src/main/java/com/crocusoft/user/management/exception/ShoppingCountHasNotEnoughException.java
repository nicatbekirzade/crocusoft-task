package com.crocusoft.user.management.exception;

import com.crocusoft.common.exception.InvalidStateException;

public class ShoppingCountHasNotEnoughException extends InvalidStateException {

    private static final long serialVersionUID = 83242081L;

    public ShoppingCountHasNotEnoughException(String message) {
        super(message);
    }
}
