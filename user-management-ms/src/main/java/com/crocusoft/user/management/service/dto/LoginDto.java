package com.crocusoft.user.management.service.dto;

import com.crocusoft.user.management.config.AuthConstants;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@ToString(exclude = "password")
public class LoginDto {

    @NotNull
    @Size(min = AuthConstants.USERNAME_MIN_LENGTH, max = AuthConstants.USERNAME_MAX_LENGTH)
    private String username;

    @NotNull
    @Size(min = AuthConstants.PASSWORD_MIN_LENGTH, max = AuthConstants.PASSWORD_MAX_LENGTH)
    private String password;

    @NotNull
    private Boolean rememberMe;

}
