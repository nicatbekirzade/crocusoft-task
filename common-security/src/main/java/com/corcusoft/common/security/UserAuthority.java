package com.corcusoft.common.security;

public enum UserAuthority {
    ANONYMOUS,
    USER,
    ADMIN,
    SUPER_USER

}
