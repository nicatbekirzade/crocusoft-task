package com.crocusoft.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCommonResponseDto {

    private Long id;
    private String fullName;
    private String username;
    private String email;
    private String phone;

}
